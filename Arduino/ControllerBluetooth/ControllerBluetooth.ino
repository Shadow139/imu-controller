/* This driver reads raw data from the BNO055

   Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3V DC
   Connect GROUND to common ground

   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <SoftwareSerial.h>  //For switching RX|TX Pins of the Arduino

/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (20)
Adafruit_BNO055 bno = Adafruit_BNO055();

//Bluetooth
#define RXD 2
#define TXD 3
SoftwareSerial BTSerial(RXD, TXD); // RX | TX


//Joystick
const int JButtonPin = 4;
const int XPin = 0;
const int YPin = 1;

//RGB LED
const int RPin = 12;
const int GPin = 11;
const int BPin = 10;

void setup(void)
{
  //Joystick
  pinMode(JButtonPin, INPUT);
  digitalWrite(JButtonPin,HIGH);

  //RGB  
  //pinMode(RPin, OUTPUT);
  //pinMode(GPin, OUTPUT);
  //pinMode(BPin, OUTPUT);
  
  Serial.begin(9600);
  BTSerial.begin(9600);  

  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }

  delay(1000);

  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  //Serial.print("Current Temperature: ");
  //Serial.print(temp);
  //Serial.println(" C");
  //Serial.println("");

  bno.setExtCrystalUse(true);
}

/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete (your own code
    should go here)
*/
/**************************************************************************/
void loop(void)
{
  // Possible vector values can be:
  // - VECTOR_ACCELEROMETER - m/s^2
  // - VECTOR_MAGNETOMETER  - uT
  // - VECTOR_GYROSCOPE     - rad/s
  // - VECTOR_EULER         - degrees
  // - VECTOR_LINEARACCEL   - m/s^2
  // - VECTOR_GRAVITY       - m/s^2
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

  sensors_event_t event;
  bno.getEvent(&event);

  //digitalWrite(RPin,LOW);
  //digitalWrite(GPin,LOW);
  //digitalWrite(BPin,LOW);


  BTSerial.print("J ");
  BTSerial.print(digitalRead(JButtonPin)); 
  BTSerial.print(" ");
  BTSerial.print(analogRead(XPin)); 
  BTSerial.print(" ");
  BTSerial.println(analogRead(YPin));
  //Serial.print("|");
  
  
  // Quaternion data
  imu::Quaternion quat = bno.getQuat();
  BTSerial.print("Q ");
  BTSerial.print(quat.x(), 4);
  BTSerial.print(" ");
  BTSerial.print(quat.y(), 4);
  BTSerial.print(" ");
  BTSerial.print(quat.z(), 4);
  BTSerial.print(" ");
  BTSerial.println(quat.w(), 4);  
  //Serial.print("|");
  
  BTSerial.print("A ");
  BTSerial.print((float)event.acceleration.x);
  BTSerial.print(" ");
  BTSerial.print((float)event.acceleration.y);
  BTSerial.print(" ");
  BTSerial.println((float)event.acceleration.z);
  //Serial.print("|");

  imu::Vector<3> lineacc = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
  BTSerial.print("L ");
  BTSerial.print(lineacc.x());
  BTSerial.print(" ");
  BTSerial.print(lineacc.y());
  BTSerial.print(" ");
  BTSerial.println(lineacc.z());
  //Serial.print("|");

  /* Display calibration status for each sensor. */
  uint8_t system, gyro, accel, mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);
  BTSerial.print("C ");
  BTSerial.print(system, DEC);
  BTSerial.print(" ");
  BTSerial.print(gyro, DEC);
  BTSerial.print(" ");
  BTSerial.print(accel, DEC);
  BTSerial.print(" ");
  BTSerial.println(mag, DEC);
  
  delay(BNO055_SAMPLERATE_DELAY_MS);
}
