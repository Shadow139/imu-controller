/* This driver reads raw data from the BNO055

   Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3V DC
   Connect GROUND to common ground

   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <SoftwareSerial.h>  //For switching RX|TX Pins of the Arduino
SoftwareSerial bluetooth(2,3); // RX, TX

/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (10)

Adafruit_BNO055 bno = Adafruit_BNO055();

const int JButtonPin = 4;
const int XPin = 0;
const int YPin = 1;

const int rPin = 9;
const int gPin = 10;
const int bPin = 11;


int cnt = 0;
bool isConnected = false;

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

/**************************************************************************/
/*
    Arduino setup function (automatically called at startup)
*/
/**************************************************************************/

void setup(void)
{

  //Joystick
  pinMode(JButtonPin, INPUT);
  digitalWrite(JButtonPin,HIGH);
  pinMode(LED_BUILTIN, OUTPUT); 
  //RGB  
  pinMode(rPin, OUTPUT);
  pinMode(gPin, OUTPUT);
  pinMode(bPin, OUTPUT);
  analogWrite(rPin,255);
  analogWrite(gPin,255);
  analogWrite(bPin,255);

  Serial.begin(38400);
  bluetooth.begin(38400);
  bluetooth.listen();

  //while(!bluetooth){}
  
  /* Initialise the sensor */
  if(!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    bluetooth.write("IOoops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    //while(1);
  }

  delay(1000);

  /* Display the current temperature */
  int8_t temp = bno.getTemp();
  //bluetooth.print("Current Temperature: ");
  //bluetooth.print(temp);
  //bluetooth.println(" C");
  //bluetooth.println("");

  bno.setExtCrystalUse(true);
  //bluetooth.println("Calibration status values: 0=uncalibrated, 3=fully calibrated");
  
  digitalWrite(LED_BUILTIN, HIGH);
  bluetooth.write("Hi from IMU Controller||");
  Serial.print("Hi from IMU Controller||");
}

void send_float (float arg) {
  // get access to the float as a byte-array:
  byte * data = (byte *) &arg; 

  // write the data to the serial
  bluetooth.write (data, sizeof (arg));
}

void send_short (short arg) {
  byte * data = (byte *) &arg;
  bluetooth.write(data, sizeof(arg));
}
/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete (your own code
    should go here)
*/
/**************************************************************************/
void loop(void)
{
  //Connection verification
  if(bluetooth.available())
  {  
    String serialInput;
    while (bluetooth.available()) {
      delay(3);  //delay to allow buffer to fill
      if (bluetooth.available() > 0) {
        char c = bluetooth.read();  //gets one byte from serial buffer
        serialInput += c; //makes the string readString
      }
    }
    Serial.println(serialInput);
    String splitInput = getValue(serialInput, ' ', 0);
    
    if(splitInput == "c"){
      if(serialInput.length() > 7)
      {
        String rVal = getValue(serialInput, ' ', 1);
        String gVal = getValue(serialInput, ' ', 2);
        String bVal = getValue(serialInput, ' ', 3);

        int r = rVal.toInt();
        int g = gVal.toInt();
        int b = bVal.toInt();

        analogWrite(rPin,r);
        analogWrite(gPin,g);
        analogWrite(bPin,b);
      }
        isConnected = true;     
     }
  }

  if(!isConnected)
  {
     //return;
  }

  // Possible vector values can be:
  // - VECTOR_ACCELEROMETER - m/s^2
  // - VECTOR_MAGNETOMETER  - uT
  // - VECTOR_GYROSCOPE     - rad/s
  // - VECTOR_EULER         - degrees
  // - VECTOR_LINEARACCEL   - m/s^2
  // - VECTOR_GRAVITY       - m/s^2
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

  sensors_event_t event;
  bno.getEvent(&event);

  uint8_t sys, gyro, accel, mag = 0;
  bno.getCalibration(&sys, &gyro, &accel, &mag);
 
  // Quaternion data  
  imu::Quaternion quat = bno.getQuat();
    bluetooth.print("q");
  send_float(quat.x());
  send_float(quat.y());
  send_float(quat.z());
  send_float(quat.w());  
  bluetooth.write(sys);
  bluetooth.write(gyro);
  bluetooth.print("|");

  //Joystick
  byte jPressed = digitalRead(JButtonPin);
  short xValue = analogRead(XPin); 
  short yValue = analogRead(YPin);
  bluetooth.print("a");
  bluetooth.write( jPressed );
  send_short( xValue );
  send_short( yValue );

    
//accel
  imu::Vector<3> lineacc = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);//(acceleration - gravity)
  send_float(lineacc.x());
  send_float(lineacc.y());
  send_float(lineacc.z());
  bluetooth.write(accel);
  bluetooth.write(mag);

  /* Display calibration status for each sensor. */

  


  //bluetooth.print("||");
  //bluetooth.flush();
}


