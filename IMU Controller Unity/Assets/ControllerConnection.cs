﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerConnection : MonoBehaviour {

    public static Controller connectiontype;

    public Controller controllerConnection;
    public GameObject Serial;
    public GameObject BLE;

    private SerialControllerCustomDelimiter serialConnection;
    private BLEControllerConnection bleConnection;


    void Awake () {
        if (controllerConnection == Controller.BluetoothLE)
        {
            Serial.SetActive(false);
            BLE.SetActive(true);
            connectiontype = controllerConnection;
            bleConnection = BLE.GetComponent<BLEControllerConnection>();
            Debug.Log("Bluetooth MODE.");
        }
        else
        {
            Serial.SetActive(true);
            BLE.SetActive(false);
            connectiontype = controllerConnection;
            serialConnection = Serial.GetComponent<SerialControllerCustomDelimiter>();
            Debug.Log("Serial MODE.");
        }
    }

    public enum Controller
    {
        Serial,
        BluetoothLE
    }

    public void SendRandomLedColor()
    {
        if (controllerConnection == Controller.BluetoothLE)
        {
            bleConnection.sendRandomLEDColor();
        }
        else
        {
            serialConnection.SendRandomLEDColor();
        }
    }

}
