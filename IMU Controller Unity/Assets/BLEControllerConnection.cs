﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class BLEControllerConnection : MonoBehaviour {

    // -----------------------------------------------------------------
    // change these to match the bluetooth device you're connecting to:
    // -----------------------------------------------------------------
    // private string _FullUID = "713d****-503e-4c75-ba94-3148f18d941e"; // redbear module pattern
    private string _FullUID = "0000****-0000-1000-8000-00805f9b34fb";     // BLE-CC41a module pattern(china HM-10)
    private string _serviceUUID = "ffe0"; // UUIDs can be read from the used bluetooth module
    private string _readCharacteristicUUID = "ffe1";
    private string _writeCharacteristicUUID = "ffe1";
    private string deviceToConnectTo = "BT05"; // name of the Bluetooth device 

    public bool isConnected = false;
    private bool _readFound = false;
    private bool _writeFound = false;
    private string _connectedID = null;

    private Dictionary<string, string> _peripheralList;
    private float _subscribingTimeout = 0f;

    public Debugging debuggingController;
    public Image ledColor;
    public Text txtConnectionInfo;
    public Text debug0;
    public Text debug1;
    public Text BLEInfo;
    public Slider rSlider;
    public Slider gSlider;
    public Slider bSlider;
    public Image sliderLedColor;


    int dataAmount = 0;
    float startTime;
    byte[] d;
    Color col;

    void Start()
    {
        txtConnectionInfo = GameObject.FindWithTag("con").GetComponent<Text>();

        txtConnectionInfo.text = "Initializing Bluetooth \n";
        Debug.Log("Initializing Bluetooth");

        BluetoothLEHardwareInterface.Initialize(true, false, () => { },
                                      (error) => { }
        );
        Invoke("scan", 1f);
    }

    void Update()
    {
        if (_readFound && _writeFound)
        {
            _readFound = false;
            _writeFound = false;
            _subscribingTimeout = 1.0f;
        }

        if (_subscribingTimeout > 0f)
        {
            _subscribingTimeout -= Time.deltaTime;
            if (_subscribingTimeout <= 0f)
            {
                _subscribingTimeout = 0f;
                BluetoothLEHardwareInterface.SubscribeCharacteristicWithDeviceAddress(
                   _connectedID, FullUUID(_serviceUUID), FullUUID(_readCharacteristicUUID),
                   (deviceAddress, notification) => {
                   },
                   (deviceAddress2, characteristic, data) => {
                       BluetoothLEHardwareInterface.Log("id: " + _connectedID);
                       if (deviceAddress2.CompareTo(_connectedID) == 0)
                       {
                           BluetoothLEHardwareInterface.Log(string.Format("data length: {0}", data.Length));
                           if (data.Length == 0)
                           {
                               Debug.Log("No Data.");
                           }
                           else
                           {
                               //string s = ASCIIEncoding.UTF8.GetString(data);
                               BluetoothLEHardwareInterface.Log("data: ");
                               receiveData(data);
                           }
                       }
                   });

            }
        }
    }

    void receiveData(byte[] message)
    {
        //txtConnectionInfo.text = "Received: " + message.Length;
        dataAmount += message.Length;
        debug0.text = "bytes: " + dataAmount + "\n";
        debug0.text += "Packets: " + dataAmount/ 40 + "\n";
        debug1.text = "byte/s: " + dataAmount / ((Time.time - startTime)) + "\n";
        debug1.text += "Packets/s: " + dataAmount / ((Time.time - startTime))/40;

       byte[] byt = new byte[1];
        byt[0]= message[0];
        string bit = Encoding.ASCII.GetString(byt);
        debug0.text += "bit is: " + bit;
        
        if (bit.Equals("q"))
        {
            float c = System.BitConverter.ToSingle(message, 1);
            float d = System.BitConverter.ToSingle(message, 5);
            float e = System.BitConverter.ToSingle(message, 9);
            float f = System.BitConverter.ToSingle(message, 13);
            int sys = (int)message[17];
            int gyr = (int)message[18];
            debuggingController.setQuaternion(c, d, e, f);
            debuggingController.setSystemCalib(sys);
            debuggingController.setGyroscopeCalib(gyr);
        }
        else if(bit.Equals("a"))
        {
            bool test = System.BitConverter.ToBoolean(message, 1);
            short a = System.BitConverter.ToInt16(message, 2);
            short b = System.BitConverter.ToInt16(message, 4);
            debuggingController.setJoystick(test, a, b);

            float g = System.BitConverter.ToSingle(message, 6);
            float h = System.BitConverter.ToSingle(message, 10);
            float i = System.BitConverter.ToSingle(message, 14);
            int acc = (int)message[18];
            int mag = (int)message[19];
            debuggingController.setAccelerometerCalib(acc);
            debuggingController.setMagnetometerCalib(mag);
            debuggingController.setAcceleration(g, h, i);
        }
    }

    void sendDataBluetooth(string sData)
    {
        if (sData.Length > 0)
        {
            byte[] bytes = ASCIIEncoding.UTF8.GetBytes(sData);
            if (bytes.Length > 0)
            {
                sendBytesBluetooth(bytes);
            }
        }
    }

    void sendBytesBluetooth(byte[] data)
    {
        BluetoothLEHardwareInterface.Log(string.Format("data length: {0} uuid {1}", data.Length.ToString(), FullUUID(_writeCharacteristicUUID)));
        BluetoothLEHardwareInterface.WriteCharacteristic(_connectedID, FullUUID(_serviceUUID), FullUUID(_writeCharacteristicUUID),
           data, data.Length, true, (characteristicUUID) => {
               BluetoothLEHardwareInterface.Log("Write succeeded");
           }
        );
    }


    void scan()
    {

        // the first callback will only get called the first time this device is seen
        // this is because it gets added to a list in the BluetoothDeviceScript
        // after that only the second callback will get called and only if there is
        // advertising data available
        txtConnectionInfo.text = ("Starting scan \r\n");
        BLEInfo.text = "Scan started.";
        Debug.Log("Starting scan");

        BluetoothLEHardwareInterface.ScanForPeripheralsWithServices(null, (address, name) => {
            BLEInfo.text += "Adding Device:" + address + " - " + name + " \r\n";
            AddPeripheral(name, address);
        }, (address, name, rssi, advertisingInfo) => {
            BLEInfo.text += "Device discovered:" + address + " - " + name + " \r\n";

        });

    }

    void AddPeripheral(string name, string address)
    {
        txtConnectionInfo.text = ("Found " + name + " \r\n");

        if (_peripheralList == null)
        {
            _peripheralList = new Dictionary<string, string>();
        }
        if (!_peripheralList.ContainsKey(address))
        {
            _peripheralList[address] = name;
            if (name.Trim().ToLower() == deviceToConnectTo.Trim().ToLower())
            {
                BLEInfo.text += "Found our device, stop scanning \n";
                Debug.Log("Found our device, stop scanning \n");
                BluetoothLEHardwareInterface.StopScan ();

                txtConnectionInfo.text = "Connecting to " + address + "\n";
                BLEInfo.text += "Connecting to " + address + "\n";
                Debug.Log("Connecting to " + address);
                connectBluetooth(address);
            }
            else
            {
                BLEInfo.text += "Device: " + address + " - " + name + "is not what we are looking for.\n";
                txtConnectionInfo.text = "Not what we're looking for \n";
                Debug.Log("Not what we're looking for");
            }
        }
        else
        {
            txtConnectionInfo.text = "No address found \n";
            Debug.Log("No address found");
        }
    }

    void connectBluetooth(string addr)
    {
        BluetoothLEHardwareInterface.ConnectToPeripheral(addr, (address) => {
        },
           (address, serviceUUID) => {
           },
           (address, serviceUUID, characteristicUUID) => {

             // discovered characteristic
             if (IsEqual(serviceUUID, _serviceUUID))
               {
                   _connectedID = address;
                   isConnected = true;
                   debuggingController.setConnected(true);

                   if (IsEqual(characteristicUUID, _readCharacteristicUUID))
                   {
                       _readFound = true;
                   }
                   if (IsEqual(characteristicUUID, _writeCharacteristicUUID))
                   {
                       _writeFound = true;
                   }

                   txtConnectionInfo.text = "Connected";
                   txtConnectionInfo.color = Color.green;
                   //string msg = "c " + 0 + " " + 255 + " " + 255;
                   //sendBytesBluetooth(ASCIIEncoding.UTF8.GetBytes(msg));

                   Debug.Log("Connection established");
                   startTime = Time.time;
                   BluetoothLEHardwareInterface.StopScan();

               }
           }, (address) => {

             // this will get called when the device disconnects
             // be aware that this will also get called when the disconnect
             // is called above. both methods get call for the same action
             // this is for backwards compatibility
             isConnected = false;
           });

    }


    void sendData(string s)
    {
        sendDataBluetooth(s);
    }

    // -------------------------------------------------------
    // some helper functions for handling connection strings
    // -------------------------------------------------------
    string FullUUID(string uuid)
    {
        return _FullUID.Replace("****", uuid);
    }

    bool IsEqual(string uuid1, string uuid2)
    {
        if (uuid1.Length == 4)
        {
            uuid1 = FullUUID(uuid1);
        }
        if (uuid2.Length == 4)
        {
            uuid2 = FullUUID(uuid2);
        }
        return (uuid1.ToUpper().CompareTo(uuid2.ToUpper()) == 0);
    }

    public void sendRandomLEDColor()
    {
        col = Controller.color[UnityEngine.Random.Range(0, Controller.color.Length - 1)];
        string msg = "c " + (int)col.r + " " + (int)col.g + " " + (int)col.b;
        txtConnectionInfo.color = Color.black;
        txtConnectionInfo.text = msg;
        sendBytesBluetooth(ASCIIEncoding.UTF8.GetBytes(msg));
        ledColor.color = new Color(255 - col.r, 255 - col.g, 255 - col.b);
        sliderLedColor.color = new Color(255 - col.r, 255 - col.g, 255 - col.b);        
    }

    public void sendSetLED()
    {
        Color c = new Color(rSlider.value, gSlider.value, bSlider.value);
        string msg = "c " + (int)(255 - rSlider.value * 255) + " " + (int)(255 - gSlider.value * 255) + " " + (int)(255 - bSlider.value * 255);
        sendBytesBluetooth(ASCIIEncoding.UTF8.GetBytes("c " + (int)(255 - rSlider.value*255) + " " + (int)(255 - gSlider.value * 255) + " " + (int)(255 - bSlider.value * 255)));
        txtConnectionInfo.text = msg;
        sliderLedColor.color = c;
        ledColor.color = c;
    }

    public void adjustColorImg()
    {
        Color c = new Color(rSlider.value, gSlider.value, bSlider.value);
        Debug.Log("old " + sliderLedColor.color + "  -  " + c + "new");
        sliderLedColor.color = c;
    }
}
