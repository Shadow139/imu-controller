﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDrawing : MonoBehaviour {

    public Camera cam;
    public Vector3 screenPoint;
    public ControllerLocator locator;

    public GameObject locatorSphere;
	void Start () {
		
	}
	
	void Update () {
        float magnitude = locator.pos_out.z / 50.0f; 
        Ray ray = cam.ScreenPointToRay(locator.pos_out);
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);

        float a = locator.pos_out.z / Mathf.PI / locator.camRatio;
        a = (a / 4.0f) * 0.755727566f;
        float distance = 0.25f / Mathf.Tan(a * 0.5f);
        //locatorSphere.transform.position = ray.origin + ray.direction * distance;
        locatorSphere.transform.position = ray.origin + ((5 * ray.direction) * (1.0f - magnitude));
        //Debug.Log("distance: " + distance);		
    }
}
