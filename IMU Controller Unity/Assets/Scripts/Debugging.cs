﻿using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;
using System.Collections;
using System.Threading;
using System;
//Check out!!
//http://stackoverflow.com/questions/40546815/read-and-write-data-to-from-serial-port-in-c-sharp
public class Debugging : MonoBehaviour {

    public enum ConnectionType
    {
        Serial,
        Bluetooth,
        Sockets
    }
    public ConnectionType connectionType;

    public GameObject controller;
    public GameObject laser;
    public Camera camera;

    [Tooltip("Will override autoFindPort if not left blank.")]
    public string PortName = "";
    public int Baud = 9600;
    public int RebootDelay = 3;
    public int ReadTimeout = 50;

    #region UI
    [Header("Textfields")]

    [SerializeField]
    private Text debugTxt0;
    [SerializeField]
    private Text debugTxt1;

    [SerializeField]
    private Image joystickImg;
    [SerializeField]
    private Image joystockBG;

    [SerializeField]
    private Text quaternionTxt;
    [SerializeField]
    private Text eventAccTxt;
    [SerializeField]
    private Text lineAccTxt;
    [SerializeField]
    private Text joystickTxt;
    [SerializeField]
    private Text calibrationTxt;

    [SerializeField]
    private Image resetImg;
    #endregion

    SerialPort sp;
    SerialPort foundPort;
    bool portFound;
    ForceMode f = ForceMode.Impulse;


    float xE, yE, zE;
    public float xL, yL, zL;
    public bool updateRotation, updatePosition;
    float xQ, yQ, zQ, wQ;
    bool btnJ;
    int xJ, yJ;
    int sys, gyr, acc, mag;
    bool connected = false;
	Rigidbody rb;

    bool scaleMode;
    bool moveMode = true;
    bool videoOn = true;
    float magnitude = 0.005f;
    float resetControllerTime = 2.0f;
    float currentAccumulatedTime = 0.0f;
    bool isJoystickPressed = false;
    public Vector3 velocity;

    Quaternion rotationOffset = new Quaternion();
    Vector3 positionOffset = new Vector3();
    //public Material matConnected;
    //public Material matDisconnected;

    private Color[] color = { /*Light Blue*/new Color(255, 0, 0), /*Pink*/new Color(0, 255, 0), /*Yellow*/new Color(0, 0, 255),
    /*Blue*/new Color(255,255,0), /*Green*/new Color(255,0,255), /*Red*/new Color(0,255,255)};

    #region Beta stuff

    [Header("Still in Beta")]
    [Tooltip("Will try to find the Port of the connected Arduino.")]
    public bool autoFindPort = false;
    #endregion

    void Start () {     
        rb = GetComponent<Rigidbody> ();
        //if (matConnected == null || matDisconnected == null)
        //{
        //    matConnected = Resources.Load("Materials/matControllerConnected") as Material;
        //    matDisconnected = Resources.Load("Materials/matControllerDisconnected") as Material;
        //}
        //Debug.Log(matConnected + "  " + matDisconnected);
    }

    void Update()
    {
        if (true)
        {
            updateUIData();

            if (updateRotation)
                updateControllerRotation();

            if (updatePosition)
                updateControllerPosition();

            //if (btnJ == false && !isJoystickPressed)
            //{
            //    isJoystickPressed = true;
            //    laser.SetActive(false);
            //}

            //if(btnJ == true && isJoystickPressed)
            //{
            //    isJoystickPressed = false;
            //    laser.SetActive(true);
            //}

            //if (scaleMode) { 
            //    if (xJ > 600)
            //    {
            //        if(controller.transform.localScale.x < 2)
            //            controller.transform.localScale = new Vector3(magnitude * (xJ * Time.deltaTime), magnitude * (xJ * Time.deltaTime), magnitude * (xJ * Time.deltaTime));
            //    }
            //    else if (xJ < 400 && yJ > 0)
            //    {
            //        if (controller.transform.localScale.x > .1)
            //            controller.transform.localScale = new Vector3(magnitude * (xJ * Time.deltaTime), magnitude * (xJ * Time.deltaTime), magnitude * (xJ * Time.deltaTime));
            //    }
            //}

            //if (moveMode)
            //{
            //    if (xJ > 600)
            //    {
            //         controller.transform.position += new Vector3(magnitude * 100 * Time.deltaTime,0,0);
            //    }
            //    else if (xJ < 400 && yJ > 0)
            //    {
            //          controller.transform.position -= new Vector3(magnitude * 100 * Time.deltaTime, 0, 0);
            //    }

            //    if (yJ > 600)
            //    {
            //         controller.transform.position += new Vector3(0, 0, magnitude * 100 * Time.deltaTime);
            //    }
            //    else if (yJ < 400 && yJ > 0)
            //    {
            //       controller.transform.position -= new Vector3(0, 0, magnitude * 100 * Time.deltaTime);
            //    }
            //}
        }
    }

    void FixedUpdate()
    {

    }

    private void ReceivedSerialHandler(object sender, SerialDataReceivedEventArgs e)
    {
        SerialPort port = (SerialPort)sender;
        string s = port.ReadExisting();
        Debug.Log(s);
        port.Close();
    }
    
    private bool readSensorDataBytes()
    {

        byte[] buffer = new byte[37];

        try
        {
            int bytesRead = sp.Read(buffer, 0, buffer.Length);

            //Debug.Log(bytesRead + "BytesRead / " + System.Text.Encoding.Default.GetString(buffer));

            byte[] j_button = { buffer[0] };
            byte[] j_x = { buffer[1], buffer [2] };
            byte[] j_y = { buffer[3], buffer [4] };

            Array.Reverse(j_button);
            Array.Reverse(j_x);
            Array.Reverse(j_y);

            btnJ = BitConverter.ToBoolean(j_button, 0);
            xJ = BitConverter.ToInt16(j_x, 0);
            yJ = BitConverter.ToInt16(j_y, 0);

            //Debug.Log(btnJ + " " + xJ + " " + yJ);

            byte[] q_x = { buffer[5], buffer[6], buffer[7], buffer[8] };
            byte[] q_y = { buffer[9], buffer[10], buffer[11], buffer[12] };
            byte[] q_z = { buffer[13], buffer[14], buffer[15], buffer[16] };
            byte[] q_w = { buffer[17], buffer[18], buffer[19], buffer[20] };

            //Array.Reverse(q_x);
            //Array.Reverse(q_y);
            //Array.Reverse(q_z);
            //Array.Reverse(q_w);

            xQ = BitConverter.ToSingle(q_x, 0);
            yQ = BitConverter.ToSingle(q_y, 0);
            zQ = BitConverter.ToSingle(q_z, 0);
            wQ = BitConverter.ToSingle(q_w, 0);

            byte[] l_x = { buffer[21], buffer[22], buffer[23], buffer[24] };
            byte[] l_y = { buffer[25], buffer[26], buffer[27], buffer[28] };
            byte[] l_z = { buffer[29], buffer[30], buffer[31], buffer[32] };

            //Array.Reverse(l_x);
            //Array.Reverse(l_y);
            //Array.Reverse(l_z);

            xL = BitConverter.ToSingle(l_x, 0);
            yL = BitConverter.ToSingle(l_y, 0);
            zL = BitConverter.ToSingle(l_z, 0);

            byte[] c_sys = { 0, buffer[33] };
            byte[] c_gyr = { 0, buffer[34] };
            byte[] c_acc = { 0, buffer[35] };
            byte[] c_mag = { 0, buffer[36] };

            Array.Reverse(c_sys);
            Array.Reverse(c_gyr);
            Array.Reverse(c_acc);
            Array.Reverse(c_mag);

            sys = BitConverter.ToInt16(c_sys, 0);
            gyr = BitConverter.ToInt16(c_gyr, 0);
            acc = BitConverter.ToInt16(c_acc, 0);
            mag = BitConverter.ToInt16(c_mag, 0);

            return true;
        }
        catch (Exception e)
        {
            Debug.Log("aaaargh " + e.GetBaseException());
        }

        return false;
    }

    private bool readSensorData()
    {
        
        if (sp != null && sp.IsOpen)
        {
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    string line = sp.ReadLine();

                    if (!string.IsNullOrEmpty(line))
                    {
                        //Debug.Log(Time.time + ":  " + line);
                        //Debug.Log(line.Length);
                        string[] split = line.Split(' ');

                        if (split[0].Length > 0)
                        {
                            switch (split[0])
                            {
                                case "Q":
                                    xQ = float.Parse(split[1]);
                                    yQ = float.Parse(split[2]);
                                    zQ = float.Parse(split[3]);
                                    wQ = float.Parse(split[4]);


                                    break;

                                case "A":
                                    xE = float.Parse(split[1]);
                                    yE = float.Parse(split[2]);
                                    zE = float.Parse(split[3]);


                                    break;

                                case "L":
                                    xL = float.Parse(split[1]);
                                    yL = float.Parse(split[2]);
                                    zL = float.Parse(split[3]);


                                    //transform.Translate(x / 10, z / 10, (y - 0.1f) / 10);

                                    break;

                                case "J":
                                    //btnJ = int.Parse(split[1]);
                                    xJ = int.Parse(split[2]);
                                    yJ = int.Parse(split[3]);

                                    break;

                                case "C":
                                    sys = int.Parse(split[1]);
                                    gyr = int.Parse(split[2]);
                                    acc = int.Parse(split[3]);
                                    mag = int.Parse(split[4]);

                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                Debug.Log("Reading sensor data timed out. Closing port " + PortName);
                sp.Close();
            }
        
        }
        return false;
    }

    private void updateUIData()
    {
        quaternionTxt.text = "Xq: " + xQ + "\n" + "Yq: " + yQ + "\n" + "Zq: " + zQ + "\n" + "Wq: " + wQ;
        lineAccTxt.text = "XAccLine: " + xL + "\n" + "YAccLine: " + yL + "\n" + "ZAccLine: " + zL;
        joystickTxt.text = "JoystickBtn: " + btnJ + "\n" + "JoystickX: " + xJ + "\n" + "JoystickY: " + yJ;
        calibrationTxt.text = "mag: " + mag + "\n" + "acc: " + acc + "\n" + "gyr: " + gyr + "\n" + "sys: " + sys;

        if (btnJ == false)
        {
            joystickImg.color = Color.green;
            transform.position = Vector3.zero;
        }
        else
        {
            joystickImg.color = Color.red;
        }

        Vector2 pos;
        pos.x = xJ;
        pos.y = yJ;

        pos.x = (pos.x) / 1024.0f;
        pos.y = (pos.y) / 1024.0f;

        joystickImg.rectTransform.anchoredPosition = new Vector3(pos.y * joystockBG.rectTransform.sizeDelta.y,
            pos.x * joystockBG.rectTransform.sizeDelta.x);
    }

    private void updateControllerRotation()
    {
        transform.rotation = new Quaternion(yQ, -zQ, -xQ, wQ);
    }

    private void updateControllerPosition()
    {
        if (Math.Abs(xL) > 0.4 || true)
        {
            velocity += new Vector3(xL * Time.deltaTime, yL * Time.deltaTime, zL * Time.deltaTime);

            controller.transform.localPosition += velocity * Time.deltaTime;
        }
        else
        {

        }
        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //   f = ForceMode.Acceleration;
        //}
        //else if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    f = ForceMode.Impulse;
        //}
        //else if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    f = ForceMode.Force;
        //}
        //else if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    f = ForceMode.VelocityChange;
        //}
        //if (acc >= 0)
        //{
        //    float xL_ = Math.Abs(xL) > 0.2 ? xL : 0;
        //    float yL_ = Math.Abs(yL) > 0.2 ? yL : 0;
        //    float zL_ = Math.Abs(zL) > 0.2 ? zL : 0;
        //    if (!rb.isKinematic)
        //    {
        //        rb.AddRelativeForce(new Vector3(-yL_, zL_, xL_), f);

        //        //rb.AddRelativeForce(new Vector3(0, 0, xL_), f);
        //        //rb.AddRelativeForce(new Vector3(0.0f,0.0f,(float)Math.Sin(Time.time) * 10), f);
        //    }
        //    else
        //    {
        //        Debug.Log("whooooo");
        //        transform.Translate(transform.right * -yL_);
        //        transform.Translate(transform.up * zL_);
        //        transform.Translate(transform.forward * xL_);
        //    }
        //}
    }

    #region Setters for Controller Sensor Data

    public void setConnected(bool c)
    {
        this.connected = c;
        resetImg.color = Color.green;
        //if (connected)
        //{
        //    controller.GetComponent<Renderer>().material = matConnected;
        //}
        //else
        //{
        //    controller.GetComponent<Renderer>().material = matDisconnected;
        //}
    }

    public void setValues(bool btnJ,
                      short xJ,
                      short yJ,
                      float xQ,
                      float yQ,
                      float zQ,
                      float wQ,
                      float xAcc,
                      float yAcc,
                      float zAcc,
                      int calibSys,
                      int calibGyr,
                      int calibAcc,
                      int calibMag)
    {
        this.btnJ = btnJ;
        this.xJ = xJ;
        this.yJ = yJ;
        this.xQ = xQ;
        this.yQ = yQ;
        this.zQ = zQ;
        this.wQ = wQ;
        this.xL = xAcc;
        this.yL = yAcc;
        this.zL = zAcc;
        this.sys = calibSys;
        this.gyr = calibGyr;
        this.acc = calibAcc;
        this.mag = calibMag;
    }

    public void setJoystick(bool btnJ,
                      short xJ,
                      short yJ)
    {
        this.btnJ = btnJ;
        this.xJ = xJ;
        this.yJ = yJ;
    }

    public void setQuaternion(float xQ,
                      float yQ,
                      float zQ,
                      float wQ)
    {
        this.xQ = xQ;
        this.yQ = yQ;
        this.zQ = zQ;
        this.wQ = wQ;
    }


    public void setAcceleration(float xAcc,
                      float yAcc,
                      float zAcc)
    {
        this.xL = xAcc;
        this.yL = yAcc;
        this.zL = zAcc;
    }

    public void setCalibration(int calibSys,
                      int calibGyr,
                      int calibAcc,
                      int calibMag)
    {
        this.sys = calibSys;
        this.gyr = calibGyr;
        this.acc = calibAcc;
        this.mag = calibMag;
    }

    public void setMagnetometerCalib(int calib)
    {
        this.mag = calib;
    }

    public void setGyroscopeCalib(int calib)
    {
        this.gyr = calib;
    }

    public void setAccelerometerCalib(int calib)
    {
        this.acc = calib;
    }

    public void setSystemCalib(int calib)
    {
        this.sys = calib;
    }

    #endregion
}
