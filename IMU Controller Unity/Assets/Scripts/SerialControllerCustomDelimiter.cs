﻿/**
 * SerialCommUnity (Serial Communication for Unity)
 * Author: Daniel Wilches <dwilches@gmail.com>
 *
 * This work is released under the Creative Commons Attributions license.
 * https://creativecommons.org/licenses/by/2.0/
 */

using UnityEngine;
using UnityEngine.UI;
using System.Threading;

/**
 * While 'SerialController' only allows reading/sending text data that is
 * terminated by new-lines, this class allows reading/sending messages 
 * using a binary protocol where each message is separated from the next by 
 * a 1-char delimiter.
 */
public class SerialControllerCustomDelimiter : MonoBehaviour
{
    public Port port;
    [Tooltip("Port name with which the SerialPort object will be created.")]
    public string portName = "COM3";

    [Tooltip("Baud rate that the serial device is using to transmit data.")]
    public int baudRate = 9600;

    [Tooltip("Reference to an scene object that will receive the events of connection, " +
             "disconnection and the messages from the serial device.")]
    public GameObject messageListener;

    [Tooltip("After an error in the serial communication, or an unsuccessful " +
             "connect, how many milliseconds we should wait.")]
    public int reconnectionDelay = 1000;

    [Tooltip("Maximum number of unread data messages in the queue. " +
             "New messages will be discarded.")]
    public int maxUnreadMessages = 1;

    
    // Internal reference to the Thread and the object that runs in it.
    protected Thread thread;
    protected SerialThread serialThread;


    // Constants used to mark the start and end of a connection. There is no
    // way you can generate clashing messages from your serial device, as I
    // compare the references of these strings, no their contents. So if you
    // send these same strings from the serial device, upon reconstruction they
    // will have different reference ids.
    public static readonly byte[] SERIAL_DEVICE_CONNECTED = new byte[] {1,2,3,4,4,3,2,1,124,124};
	public static readonly byte[] SERIAL_DEVICE_DISCONNECTED = {4,3,2,1,1,2,3,4,124,124};

	static bool ByteArrayCompare(byte[] a1, byte[] a2)
	{
		if (a1.Length != a2.Length)
			return false;

		for (int i=0; i<a1.Length; i++)
			if (a1[i]!=a2[i])
				return false;

		return true;
	}

    // ------------------------------------------------------------------------
    // Invoked whenever the SerialController gameobject is activated.
    // It creates a new thread that tries to connect to the serial device
    // and start reading from it.
    // ------------------------------------------------------------------------
    void OnEnable()
    {
        messageListener = this.gameObject;
        checkPort();
        Text txt = GameObject.FindWithTag("con").GetComponent<Text>();

        if (txt != null)
            txt.text = "Initializing Serial \n";

        serialThread = new SerialThread(portName,
                                        baudRate,
                                        reconnectionDelay,
                                        maxUnreadMessages);
        thread = new Thread(new ThreadStart(serialThread.RunForever));
        thread.Start();
    }

    // ------------------------------------------------------------------------
    // Invoked whenever the SerialController gameobject is deactivated.
    // It stops and destroys the thread that was reading from the serial device.
    // ------------------------------------------------------------------------
    void OnDisable()
    {
        // If there is a user-defined tear-down function, execute it before
        // closing the underlying COM port.
        if (userDefinedTearDownFunction != null)
            userDefinedTearDownFunction();

        // The serialThread reference should never be null at this point,
        // unless an Exception happened in the OnEnable(), in which case I've
        // no idea what face Unity will make.
        if (serialThread != null)
        {
            serialThread.RequestStop();
            serialThread = null;
        }

        // This reference shouldn't be null at this point anyway.
        if (thread != null)
        {
            thread.Join();
            thread = null;
        }
    }

    // ------------------------------------------------------------------------
    // Polls messages from the queue that the SerialThread object keeps. Once a
    // message has been polled it is removed from the queue. There are some
    // special messages that mark the start/end of the communication with the
    // device.
    // ------------------------------------------------------------------------
    void Update()
    {
        // If the user prefers to poll the messages instead of receiving them
        // via SendMessage, then the message listener should be null.
        if (messageListener == null)
            return;

        // Read the next message from the queue
        byte[] message = ReadSerialMessage();
        if (message == null)
            return;
		
		// Check if the message is plain data or a connect/disconnect event.
		if (ByteArrayCompare (message, SERIAL_DEVICE_CONNECTED)) {
			messageListener.SendMessage ("OnConnectionEvent", true);
		} else if (ByteArrayCompare (message, SERIAL_DEVICE_DISCONNECTED)) {
			messageListener.SendMessage ("OnConnectionEvent", false);
		} else {
			messageListener.SendMessage ("OnMessageArrived", message);
		}

    }

    // ------------------------------------------------------------------------
    // Returns a new unread message from the serial device. You only need to
    // call this if you don't provide a message listener.
    // ------------------------------------------------------------------------
    public byte[] ReadSerialMessage()
    {
        // Read the next message from the queue
        return (byte[]) serialThread.ReadMessage();
    }

    // ------------------------------------------------------------------------
    // Puts a message in the outgoing queue. The thread object will send the
    // message to the serial device when it considers it's appropriate.
    // ------------------------------------------------------------------------
    public void SendSerialMessage(byte[] message)
    {
        serialThread.SendMessage(message);
    }

    // ------------------------------------------------------------------------
    // Executes a user-defined function before Unity closes the COM port, so
    // the user can send some tear-down message to the hardware reliably.
    // ------------------------------------------------------------------------
    public delegate void TearDownFunction();
    private TearDownFunction userDefinedTearDownFunction;
    public void SetTearDownFunction(TearDownFunction userFunction)
    {
        this.userDefinedTearDownFunction = userFunction;
    }

    public void SendRandomLEDColor()
    {
        Color c = Controller.color[UnityEngine.Random.Range(0, Controller.color.Length - 1)];
        messageListener.GetComponent<ByteMessageListener>().serialController.SendSerialMessage(System.Text.Encoding.ASCII.GetBytes ("Okay " + c.r + " " + c.g + " " + c.b));
    }

    public void sendSetLED()
    {
        //Color c = new Color(rSlider.value, gSlider.value, bSlider.value);
        //string msg = "c " + (int)(255 - rSlider.value * 255) + " " + (int)(255 - gSlider.value * 255) + " " + (int)(255 - bSlider.value * 255);
        //sendBytesBluetooth(ASCIIEncoding.UTF8.GetBytes("c " + (int)(255 - rSlider.value * 255) + " " + (int)(255 - gSlider.value * 255) + " " + (int)(255 - bSlider.value * 255)));
        //txtConnectionInfo.text = msg;
        //sliderLedColor.color = c;
        //ledColor.color = c;
    }
    public void checkPort()
    {
        switch (port)
        {
            case Port.UseString:
                break;
            case Port.COM1:
                portName = "COM1";
                break;
            case Port.COM2:
                portName = "COM2";
                break;
            case Port.COM3:
                portName = "COM3";
                break;
            case Port.COM4:
                portName = "COM4";
                break;
            case Port.COM5:
                portName = "COM5";
                break;
            case Port.COM6:
                portName = "COM6";
                break;
            case Port.COM7:
                portName = "COM7";
                break;
            case Port.COM8:
                portName = "COM8";
                break;
            case Port.COM9:
                portName = "COM9";
                break;
            case Port.COM10:
                //portName = "\\.\COM10";
                break;
            case Port.COM11:
                //portName = "\\.\COM11";
                break;
            case Port.COM12:
                //portName = "\\.\COM12";
                break;
            case Port.COM13:
                //portName = "\\.\COM13";
                break;
            case Port.COM14:
                //portName = "\\.\COM14";
                break;
            case Port.COM15:
                //portName = "\\.\COM15";
                break;
            default:
                break;
        }
    }

    public enum Port
    {
        UseString,
        COM1,
        COM2,
        COM3,
        COM4,
        COM5,
        COM6,
        COM7,
        COM8,
        COM9,
        COM10,
        COM11,
        COM12,
        COM13,
        COM14,
        COM15
    }
}
