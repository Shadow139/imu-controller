﻿using UnityEngine;
using System.Collections;

public class LaserPointer : MonoBehaviour {

    public LineRenderer laser;
    public Light point;
    public Light hitLight;

    private Ray ray;
    private RaycastHit hit;

    void Start()
    {
        laser = GetComponent<LineRenderer>();
        point  = GetComponent<Light>();
        hitLight = transform.Find("HitLight").GetComponent<Light>();        
    }

    void Update()
    {
        ray = new Ray(laser.transform.position, laser.transform.forward);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            hitLight.transform.position = hit.point;
            laser.SetPosition(0, transform.position);
            laser.SetPosition(1, hit.point);
        }
        else
        {
            hitLight.transform.position = ray.direction * 1000;
            laser.SetPosition(0, transform.position);
            laser.SetPosition(1, ray.direction * 1000);
        }
    }
}
