﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ControllerLocator : MonoBehaviour {

    public Vector3 pos_out;
    public Color c;
    public RawImage background;
    public Material boxShader;
    public bool outputDebug;

    private bool camAvailable;
    private WebCamTexture usedCam;
    private bool camSizeInitialized = false;
    private Texture2D readBack;
    private Texture2D test;
    
    private List<RenderTexture> rts;
    private int cyc = 0;
    private bool filterCamera = true;
    public float camRatio;
    public float maxRatio;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    IEnumerator LoadDevice(string newDevice)
    {
        UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        UnityEngine.XR.XRSettings.enabled = true;
    }

    // Use this for initialization
    private void Start()
    {
        if (UnityEngine.XR.XRSettings.loadedDeviceName == "cardboard")
        {
            //StartCoroutine(LoadDevice("None"));
        }

        c = background.material.GetColor("_MyColor");

        WebCamTexture backCam = null, frontCam = null;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            //Debug.Log("No Cameras detected");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, 1024, 1024);
            }
            else
            {
                frontCam = new WebCamTexture(devices[i].name, 1024, 1024);
            }
        }

        //usedCam.requestedHeight = 1024;
        //usedCam.requestedWidth = 1024;

        usedCam = backCam;
        if (usedCam == null)
        {
            usedCam = frontCam;
        }

        camRatio = usedCam.width / usedCam.height;
        camRatio = 4.0f / camRatio;
        maxRatio = Mathf.Pow(Mathf.Min(usedCam.width, usedCam.height),2) * Mathf.PI;
        maxRatio = maxRatio / (usedCam.width * usedCam.height);
        maxRatio *= 64.0f;

        usedCam.Play();
        test = Resources.Load("test") as Texture2D;
        //background.material.mainTexture = test;
        background.texture = usedCam;

        camAvailable = true;

        rts = new List<RenderTexture>();
    }

    public void cycle()
    {
        cyc = (cyc + 1) % 3;
        if (cyc == 1)
            usedCam.Stop();
        else
            usedCam.Play();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            toggleFiltering();
        }

        if (!camAvailable)
            return;

        ////Debug.Log (usedCam.width + ", " + usedCam.height);
        float ratio = (float)usedCam.width / (float)usedCam.height;
        //fit.aspectRatio = ratio;


        if (!camSizeInitialized)
        {
            readBack = new Texture2D(8, 8, TextureFormat.RGBAHalf, false);
            camSizeInitialized = true;
            int larger = Mathf.Max(usedCam.width, usedCam.height);
            int po2 = Mathf.CeilToInt(Mathf.Log(larger, 2));

            for (int i = po2 - 1; i >= 3; i--)
            {
                int po2Size = (int)Mathf.Pow(2, i);
                rts.Add(new RenderTexture(po2Size, po2Size, 0, RenderTextureFormat.ARGBHalf));
            }
        }


        float scaleY = usedCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, -1f);

        int orient = -usedCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);

        RenderTexture rt = RenderTexture.GetTemporary(usedCam.width, usedCam.height);

        //Graphics.Blit(usedCam, rt, background.material);
        for (int i = 0; i < rts.Count; i++)
        {
            if (i == 0)
            {
                if (cyc != 2)
                    Graphics.Blit(usedCam, rts[i], background.material);
            }
            else
            {
                Graphics.Blit(rts[i - 1], rts[i], boxShader);
            }
        }


        RenderTexture.active = rts[rts.Count - 1];

        readBack.ReadPixels(new Rect(0, 0, 8, 8), 0, 0);
        //readBack.ReadPixels (wholeImg, 0, 0);
        RenderTexture.active = null;

        RenderTexture.ReleaseTemporary(rt);

        if (readBack == null)
            return;

        Color[] pixelData = readBack.GetPixels();

        if (Input.GetKeyUp(KeyCode.O))
        {
            SavePNG(readBack);
        }

        float sumX = 0.0f;
        float sumY = 0.0f;
        float sumZ = 0.0f;
        for (int i = 0; i < pixelData.Length; i++)
        {
            sumX += pixelData[i].r;
            sumY += pixelData[i].g;
            sumZ += pixelData[i].b;
        }
        if (sumZ > 0)
        {
            pos_out = new Vector3(usedCam.width * sumX / sumZ, usedCam.height * sumY / sumZ, sumZ);
            if (outputDebug)
            {
                Debug.Log("#Colored Pixels: " + sumZ + ", Centroid: " + usedCam.width * sumX / sumZ + ", " + usedCam.height * sumY / sumZ);
            }
        }
        else
        {
        }

        background.material.SetColor("_MyColor", c);
    }

    public Vector2 MapMouseToCameraPosition(Vector2 mousePosition, Vector2 camSize)
    {
        float screenRatio = ((float)Screen.width / Screen.height);     // container ratio
        float camRatio = (camSize.x / camSize.y);

        int dist;

        Vector2 textureCoords, final;

        if (screenRatio > camRatio)
        {
            final.x = (int)(Screen.height * camRatio);
            final.y = Screen.height;
            dist = (int)((final.x - Screen.width) / 2);
            textureCoords.x = ((mousePosition.x + dist) / final.x) * camSize.x;
            textureCoords.y = (mousePosition.y / final.y) * camSize.y;
        }
        else
        {
            final.x = Screen.width;
            final.y = (int)(Screen.width / camRatio);
            dist = (int)((final.y - Screen.height) / 2);
            textureCoords.x = (mousePosition.x / final.x) * camSize.x;
            textureCoords.y = ((mousePosition.y + dist) / final.y) * camSize.y;
        }

        textureCoords.x = (int)textureCoords.x;
        textureCoords.y = (int)textureCoords.y;
        return textureCoords;
    }

    public void toggleFiltering()
    {
        if (filterCamera)
        {
            background.material.SetFloat("_MyBool", 1.0f);
            filterCamera = !filterCamera;
        }
        else
        {
            background.material.SetFloat("_MyBool", 0.0f);
            filterCamera = !filterCamera;
        }
    }

    void SavePNG(Texture2D texture)
    {
        // Encode texture into PNG
        byte[] bytes = texture.EncodeToPNG();

        // For testing purposes, also write to a file in the project folder
        File.WriteAllBytes(Application.dataPath + "/../Saved/tex_" + ".png", bytes);

        Debug.Log("Saved " + texture.name + " as .png");
    }
}

