﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

public class SceneChanger : MonoBehaviour {

	void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

	public void VRMode()
    {
		SceneManager.LoadScene("GVRDebugging", LoadSceneMode.Single);
		ToggleVR ();
    }

    public void ShaderScene()
    {
        SceneManager.LoadScene("ShaderTestScene", LoadSceneMode.Single);
		ToggleVR ();
    }

	void ToggleVR() {

        foreach (string s in UnityEngine.XR.XRSettings.supportedDevices) {
            Debug.Log(s);
        }

		if (UnityEngine.XR.XRSettings.loadedDeviceName == "cardboard") {
			StartCoroutine(LoadDevice("None"));
		} else {
			StartCoroutine(LoadDevice("cardboard"));
		}
	}

	IEnumerator LoadDevice(string newDevice)
	{
		UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);
		yield return null;
		UnityEngine.XR.XRSettings.enabled = true;
	}
}
