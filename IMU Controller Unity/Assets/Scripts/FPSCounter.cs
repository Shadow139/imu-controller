﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{

    // Attach this to a GUIText to make a frames/second indicator.
    //
    // It calculates frames/second over each updateInterval,
    // so the display does not keep changing wildly.
    //
    // It is also fairly accurate at very low FPS counts (<10).
    // We do this not by simply counting frames per interval, but
    // by accumulating FPS for each frame. This way we end up with
    // correct overall FPS even if the interval renders something like
    // 5.5 frames.

    public float updateInterval = 0.5F;

    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval
    private float min = float.MaxValue; // FPS accumulated over the interval
    private float max = float.MinValue; // FPS accumulated over the interval

    [SerializeField]
    private Text guiText;
    [SerializeField]
    private Text minText;
    [SerializeField]
    private Text maxText;

    void Start()
    {
        if (!guiText)
        {
            enabled = false;
        }
        timeleft = updateInterval;
    }

    void Update()
    {
        timeleft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        // Interval ended - update GUI text and start new interval
        if (timeleft <= 0.0)
        {
            // display two fractional digits (f2 format)
            float fps = accum / frames;

            if (min > fps)
                min = fps;
            if (max < fps)
                max = fps;

            guiText.text = System.String.Format("{0:F2} FPS", fps);

            if(minText != null)
            {
                minText.text = System.String.Format("{0:F2} MIN", min);
                minText.color = Color.red;

            }
            if (maxText != null)
            {
                maxText.text = System.String.Format("{0:F2} MAX", max);
                maxText.color = Color.green;
            }


            if (fps < 30)
                guiText.color = Color.yellow;
            else
                if (fps < 10)
                guiText.color = Color.red;
            else
                guiText.color = Color.green;
            //	DebugConsole.Log(format,level);
            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
        }
    }
}
