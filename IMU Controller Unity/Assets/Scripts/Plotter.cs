﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Plotter : MonoBehaviour {

	public Debugging pointProvider;
	public int n = 100;
	public int xyz = 0;

	private Queue<Vector3> points;
	private float step;
	private LineRenderer lr;
	private int outOfBounds = 0;


	// Use this for initialization
	void Start () {
		points = new Queue<Vector3>();
		step = 1.0f / n;
		lr = GetComponent<LineRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (points.Count == n) {
			points.Dequeue ();
			outOfBounds++;
		}

		float x = (points.Count + outOfBounds) * step;
		float y = 0;

		switch (xyz) {
		case 0:
			y = pointProvider.xL;
			break;
		case 1:
			y = pointProvider.yL;
			break;
		case 2:
			y = pointProvider.zL;
			break;
        case 3:
            y = pointProvider.velocity.x;
            break;
        case 4:
            y = pointProvider.velocity.y;
            break;
        case 5:
            y = pointProvider.velocity.z;
            break;
        case 6:
            y = pointProvider.controller.transform.position.x;
            break;
        case 7:
            y = pointProvider.controller.transform.position.y;
            break;
        case 8:
            y = pointProvider.controller.transform.position.z;
            break;

        }

		points.Enqueue (new Vector3(x, y/10, 0));

		Vector3[] pointArray = points.ToArray();

		for (int i = 0, m = pointArray.Length; i < m; i++) {
			pointArray [i].x -= outOfBounds * step;
		}

		lr.SetPositions (pointArray);
	}
}
