﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GVRDisabler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (UnityEngine.XR.XRSettings.loadedDeviceName == "cardboard")
        {
            StartCoroutine(LoadDevice("None"));
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator LoadDevice(string newDevice)
    {
        UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        UnityEngine.XR.XRSettings.enabled = true;
    }
}
