﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ByteMessageListener : MonoBehaviour {

	public SerialControllerCustomDelimiter serialController;
    public Debugging debuggingController;

    // Invoked when a line of data is received from the serial device.
    void OnMessageArrived(byte[] message)
    {
        string str_msg = System.Text.Encoding.ASCII.GetString(message);

        if (Equals (str_msg, "Hi from IMU Controller")) {
            Color c = Controller.color[UnityEngine.Random.Range(0, Controller.color.Length - 1)];
            //serialController.SendSerialMessage (System.Text.Encoding.ASCII.GetBytes ("Okay " + c.r + " " + c.g + " " + c.b));
            serialController.SendSerialMessage (System.Text.Encoding.ASCII.GetBytes ("Okay " + 0 + " " + 255 + " " + 255));
		} else {
			bool test = System.BitConverter.ToBoolean(message, 0);
			short a = System.BitConverter.ToInt16(message, 1);
			short b = System.BitConverter.ToInt16(message, 3);

			float c = System.BitConverter.ToSingle(message, 5);
			float d = System.BitConverter.ToSingle(message, 9);
			float e = System.BitConverter.ToSingle(message, 13);
			float f = System.BitConverter.ToSingle(message, 17);

			float g = System.BitConverter.ToSingle(message, 21);
			float h = System.BitConverter.ToSingle(message, 25);
			float i = System.BitConverter.ToSingle(message, 29);

			int j = (int)message [33];
			int k = (int)message [34];
			int l = (int)message [35];
			int m = (int)message [36];

            debuggingController.setValues(test, a, b, c, d, e, f, g, h, i, j, k, l, m);
            //Debug.Log ("Received: " + message.Length);
		}

	}

	// Invoked when a connect/disconnect event occurs. The parameter 'success'
	// will be 'true' upon connection, and 'false' upon disconnection or
	// failure to connect.
	void OnConnectionEvent(bool success)
	{
        Text txt = GameObject.FindWithTag("con").GetComponent<Text>();

        if (success) {
			Debug.Log ("Connection established");
            if (txt != null)
            {
                txt.text = "Connected";
                txt.color = Color.green;
            }
        } else {
			Debug.Log ("Disconnected");
            if (txt != null)
            {
                txt.text = "Disconnected";
                txt.color = Color.red;
            }
        }
	}
}
