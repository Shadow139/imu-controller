﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    Rigidbody rb;
    public bool updateRotation, updatePosition;


    float xE, yE, zE;
    public float xL, yL, zL;
    float xQ, yQ, zQ, wQ;
    bool btnJ;
    int xJ, yJ;
    int sys, gyr, acc, mag;
    bool connected = false;

    bool isJoystickPressed = false;

    public static Color[] color = { /*Light Blue*/new Color(255, 0, 0), /*Pink*/new Color(0, 255, 0), /*Yellow*/new Color(0, 0, 255),
    /*Blue*/new Color(255,255,0), /*Green*/new Color(255,0,255), /*Red*/new Color(0,255,255)};

    void Start () {
		
	}
	
	void Update () {
		
	}

    public bool GetJoystickDown()
    {
        return btnJ;
    }

    public bool GetJoystickPressed()
    {
        if(btnJ && !isJoystickPressed)
        {
            isJoystickPressed = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool GetJoystickUp()
    {
        if(!btnJ && isJoystickPressed)
        {
            isJoystickPressed = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public int GetJoystickAxis(string axis)
    {
        if (axis.Equals("X"))
        {
            return xJ;
        }

        if (axis.Equals("Y"))
        {
            return yJ;
        }

        return -1;
    }


    #region Setters for Controller Sensor Data

    public void setConnected(bool c)
    {
        this.connected = c;
    }

    public void setValues(bool btnJ,
                      short xJ,
                      short yJ,
                      float xQ,
                      float yQ,
                      float zQ,
                      float wQ,
                      float xAcc,
                      float yAcc,
                      float zAcc,
                      int calibSys,
                      int calibGyr,
                      int calibAcc,
                      int calibMag)
    {
        this.btnJ = btnJ;
        this.xJ = xJ;
        this.yJ = yJ;
        this.xQ = xQ;
        this.yQ = yQ;
        this.zQ = zQ;
        this.wQ = wQ;
        this.xL = xAcc;
        this.yL = yAcc;
        this.zL = zAcc;
        this.sys = calibSys;
        this.gyr = calibGyr;
        this.acc = calibAcc;
        this.mag = calibMag;
    }

    public void setJoystick(bool btnJ,
                      short xJ,
                      short yJ)
    {
        this.btnJ = btnJ;
        this.xJ = xJ;
        this.yJ = yJ;
    }

    public void setQuaternion(float xQ,
                      float yQ,
                      float zQ,
                      float wQ)
    {
        this.xQ = xQ;
        this.yQ = yQ;
        this.zQ = zQ;
        this.wQ = wQ;
    }


    public void setAcceleration(float xAcc,
                      float yAcc,
                      float zAcc)
    {
        this.xL = xAcc;
        this.yL = yAcc;
        this.zL = zAcc;
    }

    public void setCalibration(int calibSys,
                      int calibGyr,
                      int calibAcc,
                      int calibMag)
    {
        this.sys = calibSys;
        this.gyr = calibGyr;
        this.acc = calibAcc;
        this.mag = calibMag;
    }

    public void setMagnetometerCalib(int calib)
    {
        this.mag = calib;
    }

    public void setGyroscopeCalib(int calib)
    {
        this.gyr = calib;
    }

    public void setAccelerometerCalib(int calib)
    {
        this.acc = calib;
    }

    public void setSystemCalib(int calib)
    {
        this.sys = calib;
    }

    #endregion
}
