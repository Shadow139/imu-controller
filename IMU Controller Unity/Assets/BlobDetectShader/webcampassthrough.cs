﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class webcampassthrough : MonoBehaviour {

    private bool camAvailable;
    private WebCamTexture backCam;
    private WebCamTexture frontCam;
    private WebCamTexture usedCam;

    public RawImage background;

    // Use this for initialization
    private void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No Cameras detected");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, 1024, 1024);
            }
            else
            {
                frontCam = new WebCamTexture(devices[i].name, 1024, 1024);
            }
        }

        //usedCam.requestedHeight = 1024;
        //usedCam.requestedWidth = 1024;

        usedCam = backCam;
        if (usedCam == null)
        {
            usedCam = frontCam;
        }

        usedCam.Play();
        //background.material.mainTexture = usedCam;

        //background.texture = usedCam;

        camAvailable = true;
        
        //Debug.Log (usedCam.width + ", " + usedCam.height);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
