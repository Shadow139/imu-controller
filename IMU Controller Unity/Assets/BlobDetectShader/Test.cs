﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Collections;

public class Test : MonoBehaviour
{

    private bool camAvailable;
    private WebCamTexture backCam;
    private WebCamTexture frontCam;
    private WebCamTexture usedCam;

    public RawImage background;
    public Text mousePos;
    public Text mousePosMapped;
    public Text screenRes;
    public Text camRes;
    public Text indicatorTxt;
    public Image indicator;


    public Material boxShader;

    public AspectRatioFitter fit;

    private Texture2D readBack;
    private Texture2D test;
    private Rect wholeImg;

    private bool pickerActive;
    private bool mouse;
    private bool camSizeInitialized = false;
    private Touch firstFinger;


    public Image currentColor;

    private List<RenderTexture> rts;
	private int cyc = 0;

	private void Awake() {
		Application.targetFrameRate = 60;
	}

    IEnumerator LoadDevice(string newDevice)
    {
        UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        UnityEngine.XR.XRSettings.enabled = true;
    }

    // Use this for initialization
    private void Start()
    {
        if (UnityEngine.XR.XRSettings.loadedDeviceName == "cardboard")
        {
            StartCoroutine(LoadDevice("None"));
        }


        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            //Debug.Log("No Cameras detected");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, 1024, 1024);
            }
            else
            {
                frontCam = new WebCamTexture(devices[i].name, 1024, 1024);
            }
        }

        //usedCam.requestedHeight = 1024;
        //usedCam.requestedWidth = 1024;

        usedCam = backCam;
        if (usedCam == null)
        {
            usedCam = frontCam;
        }

        usedCam.Play();
        test = Resources.Load("test") as Texture2D;
        //background.material.mainTexture = test;
        background.texture = usedCam;

        camAvailable = true;
        pickerActive = false;

        try
        {
            firstFinger = Input.GetTouch(0);
            mouse = false;
        }
        catch (System.Exception ex)
        {
            mouse = true;
        }

        //Debug.Log("Using " + (mouse ? "Mouse" : "Touch") + " Interface");
        ////Debug.Log (usedCam.width + ", " + usedCam.height);


        rts = new List<RenderTexture>();
    }

	public void cycle() {
		cyc = (cyc + 1) % 3;
		if (cyc == 1)
			usedCam.Stop ();
		else
			usedCam.Play ();
	}

    // Update is called once per frame
    private void Update()
    {
        camRes.text = "does it happen?";


        if (!camAvailable)
            return;


        screenRes.text ="Yes";

        ////Debug.Log (usedCam.width + ", " + usedCam.height);
        float ratio = (float)usedCam.width / (float)usedCam.height;
        fit.aspectRatio = ratio;

        mousePos.text = "Maybe";

        if (!camSizeInitialized)
        {
            readBack = new Texture2D(8, 8, TextureFormat.RGBAHalf, false);
            camSizeInitialized = true;
            camRes.text = usedCam.width + "x" + usedCam.height + "-" + ((float)usedCam.width / usedCam.height);
            int larger = Mathf.Max(usedCam.width, usedCam.height);
            int po2 = Mathf.CeilToInt(Mathf.Log(larger, 2));

            for (int i = po2 - 1; i >= 3; i--)
            {
                int po2Size = (int)Mathf.Pow(2, i);
                rts.Add(new RenderTexture(po2Size, po2Size, 0, RenderTextureFormat.ARGBHalf));
            }
        }


        float scaleY = usedCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, -1f);

        int orient = -usedCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);

        RenderTexture rt = RenderTexture.GetTemporary(usedCam.width, usedCam.height);

        //Graphics.Blit(usedCam, rt, background.material);
        for (int i = 0; i < rts.Count; i++)
        {
            if (i == 0)
            {
				if ( cyc != 2 )
                	Graphics.Blit(usedCam, rts[i], background.material);
            }
            else
            {
                Graphics.Blit(rts[i - 1], rts[i], boxShader );
            }
        }


        RenderTexture.active = rts[rts.Count - 1];

        screenRes.text = Screen.width + "x" + Screen.height + "-" + ((float)Screen.width / Screen.height);
        mousePos.text = Input.mousePosition.x + "x" + Input.mousePosition.y;

        readBack.ReadPixels(new Rect(0, 0, 8, 8), 0, 0);
        //readBack.ReadPixels (wholeImg, 0, 0);
        RenderTexture.active = null;

        RenderTexture.ReleaseTemporary(rt);
    }

    private void OnPostRender()
    {

        if (pickerActive)
        {
            Vector2 pos = mouse ? new Vector2(Input.mousePosition.x, Input.mousePosition.y) : firstFinger.position;

            pos = sizsizsizsize(pos, new Vector2(usedCam.width, usedCam.height));
            mousePosMapped.text = pos.x + "x" + pos.y;
			Color c = usedCam.GetPixel((int)pos.x, (int)pos.y);
			currentColor.color = c;
            background.material.SetColor("_MyColor", c);

            //Debug.Log("mouse: " + Input.mousePosition);
            //Debug.Log(Screen.width + ", " + Screen.height);
            //Debug.Log(background.rectTransform.rect);


            if ((mouse && Input.GetMouseButton(0)) || (!mouse && firstFinger.phase == TouchPhase.Began))
            {
                pickerActive = false;
                //Debug.Log("Picker is INACTIVE!");
                background.material.SetFloat("_MyBool", 1.0f);
            }

            if ((mouse && !Input.GetMouseButton(0)) || (!mouse && firstFinger.phase == TouchPhase.Ended))
            {
                //background.material.SetColor( "_MyColor", c);
            }
            indicatorTxt.text = "Picker active";
        }
        else
        {
            indicatorTxt.text = "Picker inactive";
            //byte[] bla = readBack.GetRawTextureData();
            if (readBack == null)
                return;

            Color[] pixelData = readBack.GetPixels();
            indicatorTxt.text = "Picker inactive2";


            float sumX = 0.0f;
            float sumY = 0.0f;
            float sumZ = 0.0f;
            for (int i = 0; i < pixelData.Length; i ++)
            {
                sumX += pixelData[i].r;
                sumY += pixelData[i].g;
                sumZ += pixelData[i].b;
            }
            if (sumZ > 0)
            {
                //Debug.Log(pixelData.Length);
                Debug.Log("#Colored Pixels: " + sumZ + ", Centroid: " + usedCam.width * sumX / sumZ + ", " + usedCam.height * sumY / sumZ);
                indicatorTxt.text = (usedCam.width * sumX / sumZ) + " - " + (usedCam.height * sumY / sumZ) + "\n" + sumZ;
                indicator.rectTransform.position = new Vector3(usedCam.width * sumX / sumZ, usedCam.width * sumY / sumZ, 0);
            }
            else
            {
                indicatorTxt.text = "Picker inactive3";
            }
        }

    }

    public void selectColorOnTap()
    {
        background.material.SetFloat("_MyBool", 0.0f);
        pickerActive = true;
        //Debug.Log("Picker is Active!");
    }

    public Vector2 sizsizsizsize(Vector2 mousePosition, Vector2 camSize)
    {
        float screenRatio = ((float)Screen.width / Screen.height);     // container ratio
        float camRatio = (camSize.x / camSize.y);

        int dist;

        Vector2 textureCoords, final;

        if (screenRatio > camRatio)
        {
            final.x = (int)(Screen.height * camRatio);
            final.y = Screen.height;
            dist = (int)((final.x - Screen.width) / 2);
            textureCoords.x = ((mousePosition.x + dist) / final.x) * camSize.x;
            textureCoords.y = (mousePosition.y / final.y) * camSize.y;
        }
        else
        {
            final.x = Screen.width;
            final.y = (int)(Screen.width / camRatio);
            dist = (int)((final.y - Screen.height) / 2);
            textureCoords.x = (mousePosition.x / final.x) * camSize.x;
            textureCoords.y = ((mousePosition.y + dist) / final.y) * camSize.y;
        }

        textureCoords.x = (int)textureCoords.x;
        textureCoords.y = (int)textureCoords.y;
        return textureCoords;
    }
}
