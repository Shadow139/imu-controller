﻿Shader "Unlit/BW"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_MyColor ("Some Color", Color) = (1,1,1,1) 
		_MyBool ("Filter?", float) = 1.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "LabColorspace.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float _MyBool;
			fixed4 _MyColor;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				return o;
			}

			float3 rgb2hsv(float3 c) {
				float4 K = float4(0.0, -1.0/3.0, 2.0/3.0, -1.0);
				float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
			    float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

			    float d = q.x - min(q.w, q.y);
			    float e = 1.0e-10;
			    return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		    }

		    float3 hsv2rgb(float3 c)
			{
			    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
			    return c.z * lerp(K.xxx, saturate(p - K.xxx), c.y);
			}

			fixed3 normalizeColor(fixed3 color)
			{
				return color / max(dot(color, fixed3(0.333,0.333,0.333)), 0.3);
			}

			

			float chromaKey(float3 camera, float3 mask) {
				float3 target = rgb2hsv(mask);
				float3 cam_hsv = rgb2hsv(camera);
				float3 weights = float3(4.,1.,2.);

				float dist = length(weights * (target - cam_hsv));
				return saturate(3. * dist - 1.5);
			}

						
			fixed4 maskPixel(fixed4 pixelColor, fixed4 maskColor)
			{
				float  d;
				fixed4 calculatedColor;

				// Compute distance between current pixel color and reference color
				//d = deltaE(rgb2lab2(pixelColor.rgb), rgb2lab2(maskColor.rgb));
				d = distance(normalizeColor(pixelColor.rgb), normalizeColor(maskColor.rgb));

				// If color difference is larger than threshold, return black.
				calculatedColor = (d < 0.8) ? fixed4(1.0, 1.0, 1.0, 1.0) : fixed4(0.0, 0.0, 0.0, 0.0);

				//Multiply color by texture
				return calculatedColor;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float d;
				fixed4 pixelColor = tex2D(_MainTex, i.uv);
				//float dist = chromaKey(pixelColor.rgb, _MyColor.rgb); 
				//return dist < 0.02 ? _MyColor : pixelColor;

				if (_MyBool > 0.5) {
					return maskPixel(pixelColor, _MyColor) * float4(i.uv.x, i.uv.y, 1.0,1.0);
				} else {
					return pixelColor;
				}
			}
			ENDCG
		}
	}
}
