﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "FX/PositionShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_MaskColor ("Mask Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			float3 normalizeColor(float3 color)
			{
				return color / max(
					dot(
						color,
						float3(0.33, 0.33,0.33)
						),
					0.3);
			}

			float4 maskPixel(float4 pixelColor, float4 maskColor, float threshold)
			{
				float d;
				float4 calculatedColor;

				d = distance(normalizeColor(pixelColor.rgb), normalizeColor(maskColor.rgb));

				calculatedColor = (d > threshold) ? float4(0,0,0,0) : float4(1,1,1,1);

				return calculatedColor;
			}

			float4 coordinateMask(float4 maskColor, float2 coordinates) 
			{
				return maskColor * float4(coordinates, 1.0, 1.0);
			}

			sampler2D _MainTex;
			float4 _MaskColor;

			float4 frag (v2f i) : SV_Target
			{
				float d;
				float4 pixelColor, maskedColor, coordinateColor;

				pixelColor = tex2D(_MainTex, i.uv);
				maskedColor = maskPixel(pixelColor, _MaskColor, 0.1);
				coordinateColor = coordinateMask(maskedColor, i.uv);

				return coordinateColor;
			}
			ENDCG
		}
	}
}
